package com.monocode.servlet;

import com.monocode.AppComponent;
import com.monocode.AppModule;
import com.monocode.DaggerAppComponent;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {

  public void contextInitialized(ServletContextEvent servletContextEvent) {
    ServletContext servletContext = servletContextEvent.getServletContext();

    AppComponent appComponent = DaggerAppComponent.builder()
        .appModule(new AppModule(servletContext))
        .build();

    servletContext.setAttribute(AppComponent.CONTEXT_ATTRIBUTE_NAME, appComponent);
  }

  public void contextDestroyed(ServletContextEvent servletContextEvent) {

  }
}
