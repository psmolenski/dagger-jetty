package com.monocode.servlet;

import com.monocode.AppComponent;
import com.monocode.rest.RestModule;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DispatchingServlet extends HttpServlet {

  private AppComponent appComponent;

  @Override
  public void init() throws ServletException {
    super.init();

    appComponent = (AppComponent) getServletContext().getAttribute(AppComponent.CONTEXT_ATTRIBUTE_NAME);
    appComponent.inject(this);
  }

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String[] split = req.getServletPath().split("/");

    String pageName = "/";
    if (split.length > 1) {
      pageName += split[1];
    }

    resp.getWriter().println("page: " + pageName);

    appComponent
        .restRouterComponentBuilder()
        .restModule(new RestModule(req, resp))
        .build()
        .handler()
        .handle();


  }
}
