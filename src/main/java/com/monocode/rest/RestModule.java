package com.monocode.rest;

import dagger.Binds;
import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;

import javax.inject.Provider;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Module
public class RestModule {

  private final HttpServletRequest httpRequest;
  private final HttpServletResponse httpResponse;

  public RestModule(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
    this.httpRequest = httpRequest;
    this.httpResponse = httpResponse;
  }

  @Provides
  @HttpRequestScope
  HttpServletRequest provideRequest() {
    return httpRequest;
  }

  @Provides
  @HttpRequestScope
  HttpServletResponse provideResponse() {
    return httpResponse;
  }

  @Provides
  @HttpRequestScope
  RestHandler provideHandler(HttpServletRequest request, Map<String, Provider<RestHandler>> handlers, PageNotFoundHandler pageNotFoundHandler) {
    String path = request.getServletPath();

    if (!handlers.containsKey(path)) {
      return pageNotFoundHandler;
    }

    return handlers.get(path).get();
  }

  @Provides
  @IntoMap
  @StringKey("/hello")
  RestHandler provideHelloHandler(HelloHandler helloHandler) {
    return helloHandler;
  }

  @Provides
  @IntoMap
  @StringKey("/")
  RestHandler provideHomePageHandler(HomePageHandler homePageHandler) {
    return homePageHandler;
  }
}
