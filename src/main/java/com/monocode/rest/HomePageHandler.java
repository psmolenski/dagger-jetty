package com.monocode.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HomePageHandler implements RestHandler {

  private final HttpServletResponse response;

  @Inject
  public HomePageHandler(HttpServletResponse response) {
    this.response = response;
  }

  @Override
  public void handle() throws IOException {
    response.getWriter().println("Home Page");
  }
}
