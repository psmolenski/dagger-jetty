package com.monocode.rest;

import dagger.Subcomponent;

@Subcomponent(modules = {RestModule.class})
@HttpRequestScope
public interface RestRouterComponent {
  RestHandler handler();

  @Subcomponent.Builder
  interface Builder {
    RestRouterComponent build();
    Builder restModule(RestModule restModule);
  }
}
