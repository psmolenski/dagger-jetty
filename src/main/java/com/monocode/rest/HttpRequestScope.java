package com.monocode.rest;

import javax.inject.Scope;

@Scope
public @interface HttpRequestScope {
}
