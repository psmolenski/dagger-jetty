package com.monocode.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PageNotFoundHandler implements RestHandler{
  private final HttpServletResponse response;

  @Inject
  public PageNotFoundHandler(HttpServletResponse response) {
    this.response = response;
  }

  @Override
  public void handle() throws IOException {
    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    response.getWriter().println("Handler not found");
  }
}
