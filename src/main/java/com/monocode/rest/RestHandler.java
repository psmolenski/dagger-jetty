package com.monocode.rest;

import java.io.IOException;

public interface RestHandler {
  void handle() throws IOException;
}
