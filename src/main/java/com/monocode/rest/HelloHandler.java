package com.monocode.rest;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloHandler implements RestHandler {

  private final HttpServletRequest request;
  private final HttpServletResponse response;
  private final ServletContext servletContext;

  @Inject
  public HelloHandler(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext) {
    this.request = request;
    this.response = response;
    this.servletContext = servletContext;
  }

  @Override
  public void handle() throws IOException {
    response.getWriter().write("Hello my friend");
  }
}
