package com.monocode;

import com.monocode.rest.RestRouterComponent;
import com.monocode.servlet.DispatchingServlet;
import dagger.Component;

import javax.inject.Singleton;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
  String CONTEXT_ATTRIBUTE_NAME = "APP_COMPONENT";

  void inject(DispatchingServlet dispatchingServlet);

  RestRouterComponent.Builder restRouterComponentBuilder();
}
