package com.monocode;

import com.monocode.rest.RestRouterComponent;
import dagger.Module;
import dagger.Provides;

import javax.servlet.ServletContext;

@Module(subcomponents = {RestRouterComponent.class})
public class AppModule {

  private final ServletContext servletContext;

  public AppModule(ServletContext servletContext) {
    this.servletContext = servletContext;
  }

  @Provides
  ServletContext provideServletContext() {
    return servletContext;
  }

}
